const express = require('express');
const mongoose = require('mongoose');
const stuffRouter = require('./stuff/routes/stuff');
const userRouter = require('./user/routeUser');


const app = express();
app.use(express.json());



mongoose.connect('mongodb+srv://hasalp:viGE3TOi1TwdW9JJ@cluster0.sdpr1.mongodb.net/?retryWrites=true&w=majority',
  { useNewUrlParser: true,
    useUnifiedTopology: true })
  .then(() => console.log('Connexion à MongoDB réussie !'))
  .catch(() => console.log('Connexion à MongoDB échouée !'));

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});
app.use('/api/stuff',stuffRouter);
app.use('/api/auth',userRouter);

module.exports = app;