const express = require('express');
const stuffCtrl = require('../controllers/stuff');
const auth = require('../../midleware/auth');


const router = express.Router();

router.post('/',auth,stuffCtrl.createThing);

router.put('/:id',auth, stuffCtrl.updateThing);

router.delete('/:id',auth, stuffCtrl.deleteThing );

router.get('/:id',auth, stuffCtrl.getOneThing);


router.get('/'+'',auth, stuffCtrl.getAllThings);




module.exports = router;